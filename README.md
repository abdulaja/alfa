# Alfa Test

This is sample project for test

## Environment Setup

### Requirements

#### + Java 8

We use Java 8 as basic language for backend development, don't forget to set JAVA_HOME into system environment.

#### + Maven

- Download and install [Apache Maven](https://maven.apache.org/). Set M2_HOME and MAVEN_HOME.
- Ask your leader to configure setting.xml from .m2 folder.

#### + Lombok

We are using [Lombok](https://projectlombok.org/) to generate boilerplate code, see how to integrate it with your favorite [IDE](https://projectlombok.org/setup/overview).

#### + PostgreSQL

Install PostgreSQL database, setup username and password for development application.yml

## How to Run

#### + Alfa Apps

- Open file initialDB.sql and show instruction
- run AlfaApplication.java as Spring boot app
- open browser and go to : http://localhost:9898/alfa

#### + TangTingTung Apps

- run TangTingTungApps.java in demo
- play app in console