package com.asiscode.alfa.service;

import com.asiscode.alfa.controller.PlayableRequest;

import java.util.List;

public interface PlayableService {

    List<Long> generateListData(PlayableRequest request);

}
