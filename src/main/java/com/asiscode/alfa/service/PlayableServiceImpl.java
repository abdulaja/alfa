package com.asiscode.alfa.service;

import com.asiscode.alfa.controller.CurrentMatch;
import com.asiscode.alfa.controller.PlayableRequest;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class PlayableServiceImpl implements PlayableService {

    @Override
    public List<Long> generateListData(PlayableRequest request) {
        List<Long> result = new ArrayList<>();
        if (request.getEndData() < request.getStartData())
            throw new RuntimeException("failed request");
        if (request.getEndData() - request.getStartData() > 50)
            throw new RuntimeException("too much data");

        for (Long i = request.getStartData(); i <= request.getEndData(); i++) {
            result.add(i);
        }
        Collections.shuffle(result);
        return result;
    }

    public void playing() {
        boolean playAgain = false;
        Scanner scanner = new Scanner(System.in);
        Long startData = null;
        Long endData = null;
        List<Long> choices;
        String choice = null;
        List<String> choicePick = Arrays.asList("l", "r");

        while (Objects.isNull(startData)) {
            System.out.println("Please enter start data : ");
            try {
                startData = scanner.nextLong();
            } catch (InputMismatchException e) {
                scanner.nextLine();
            }
        }

        while (Objects.isNull(endData)) {
            System.out.println("Please enter end data : ");
            try {
                endData = scanner.nextLong();
            } catch (InputMismatchException e) {
                scanner.nextLine();
            }
        }

        choices = generateListData(PlayableRequest.builder().startData(startData).endData(endData).build());

        CurrentMatch match = CurrentMatch.builder()
                .choices(choices)
                .playerChoices(new ArrayList<>())
                .aiChoices(new ArrayList<>())
                .playerSum(0L)
                .aiSum(0L)
                .build();
        while (!choices.isEmpty()) {
            System.out.println("--------------------------------------------------------------------");
            System.out.println("Choices : " + choices.toString());
            while (Objects.isNull(choice)) {
                System.out.println("Please enter your choice (L/R) : ");
                choice = scanner.next();
                if (!choice.equalsIgnoreCase("r") && !choice.equalsIgnoreCase("l")) {
                    choice = null;
                    System.out.println("illegal choice");
                    scanner.nextLine();
                }
            }
            play(match, choice, "player");
            System.out.println("Your choice : " + match.getPlayerChoice());
            System.out.println("Your total : " + match.getPlayerSum());
            choices = match.getChoices();
            choice = null;

            // ai turn
            if (!Objects.isNull(match.getLeftVal()) && !Objects.isNull(match.getRightVal())) {
                System.out.println("--------------------------------------------------------------------");
                System.out.println("Choices : " + choices.toString());
                choice = getAiChoice(match, choicePick);
                play(match, choice, "ai");
                System.out.println("AI choice : " + match.getAiChoice());
                System.out.println("AI total : " + match.getAiSum());
                choices = match.getChoices();
                choice = null;
            }

        }

        System.out.println("====================================================================");
        System.out.println("Your choices : " + match.getPlayerChoices());
        System.out.println("Your total : " + match.getPlayerSum());
        System.out.println("AI choices : " + match.getAiChoices());
        System.out.println("AI total : " + match.getAiSum());
        System.out.println("Result : " + match.getResult());

        choice = null;
        if (scanner != null)
            scanner.close();
    }

    private void play(CurrentMatch match, String choice, String player) {
        if (match.getChoices().isEmpty())
            throw new RuntimeException("no choices");

        if (choice.equalsIgnoreCase("r")) {
            setPlayerChoice(match, player, match.getChoices().get(match.getChoices().size()-1));
            match.getChoices().remove(match.getChoices().size()-1);
        } else if (choice.equalsIgnoreCase("l")) {
            setPlayerChoice(match, player, match.getChoices().get(0));
            match.getChoices().remove(0);
        } else {
            throw new RuntimeException("wrong choice");
        }

        Collections.shuffle(match.getChoices());
        if (!match.getChoices().isEmpty()) {
            match.setLeftVal(match.getChoices().get(0));
            match.setRightVal(match.getChoices().get(match.getChoices().size()-1));
        } else {
            match.setLeftVal(null);
            match.setRightVal(null);
            getResult(match);
        }
    }

    private String getAiChoice(CurrentMatch match, List<String> choicePick) {
        if (match.getChoices().size() < 5) {
            return match.getLeftVal() > match.getRightVal() ? "l" : "r";
        } else {
            Random rand = new Random();
            return choicePick.get(rand.nextInt(choicePick.size()));
        }
    }

    private void setPlayerChoice(CurrentMatch match, String player, Long choice) {
        if (player.equalsIgnoreCase("ai")) {
            match.getAiChoices().add(choice);
            match.setAiChoice(choice);
            match.setAiSum(match.getAiSum() + choice);
        } else {
            match.getPlayerChoices().add(choice);
            match.setPlayerChoice(choice);
            match.setPlayerSum(match.getPlayerSum() + choice);
        }
    }

    private void getResult(CurrentMatch match) {
        if (match.getAiSum() > match.getPlayerSum()) {
            match.setResult("You lose!");
        } else {
            match.setResult("You win!");
        }
    }
}
