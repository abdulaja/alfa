package com.asiscode.alfa.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Id;
import javax.persistence.MappedSuperclass;

/**
 * @author Abdulaja
 */
@Getter
@Setter
@MappedSuperclass
public abstract class BaseEntity {

    @Id
    /*@GeneratedValue(generator = "base_generator")
    @SequenceGenerator(
            name = "base_generator",
            sequenceName = "base_sequence",
            initialValue = 100
    )*/
    private Long id;

}
