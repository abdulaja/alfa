package com.asiscode.alfa.controller;

import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
public class PlayableRequest {

    private Long startData;
    private Long endData;

}
