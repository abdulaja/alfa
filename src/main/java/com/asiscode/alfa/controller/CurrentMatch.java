package com.asiscode.alfa.controller;

import lombok.Builder;
import lombok.Data;

import java.util.List;

@Data
@Builder
public class CurrentMatch {

    private String history;

    private List<Long> choices;
    private Long leftVal;
    private Long rightVal;

    private Long playerChoice;
    private Long aiChoice;
    private List<Long> playerChoices;
    private List<Long> aiChoices;
    private Long playerSum;
    private Long aiSum;
    private String result;

}
