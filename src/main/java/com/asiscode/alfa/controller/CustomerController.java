package com.asiscode.alfa.controller;

import com.asiscode.alfa.entity.Customer;
import com.asiscode.alfa.repo.CustomerRepo;
import com.asiscode.alfa.service.PlayableService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RequiredArgsConstructor
@Controller
@RequestMapping("/customers")
public class CustomerController {

    private final CustomerRepo customerRepo;
    private final PlayableService playableService;

    @GetMapping
    public String index(Model model) {
        model.addAttribute("customers", customerRepo.findAll());
        return "customer/index";
    }

    @GetMapping("/sign-up")
    public String showSignUpForm(Customer customer) {
        return "customer/add-customer";
    }

    @GetMapping("/{id}")
    public String showUpdateForm(@PathVariable("id") long id, Model model) {
        Customer customer = customerRepo.findById(id)
                .orElseThrow(() -> new IllegalArgumentException("Invalid customer Id:" + id));

        model.addAttribute("customer", customer);
        return "customer/update-customer";
    }

    @PostMapping
    public String add(@Valid Customer customer, BindingResult result, Model model) {
        if (result.hasErrors()) {
            return "customer/add-customer";
        }

        customerRepo.save(customer);
        model.addAttribute("customers", customerRepo.findAll());
        return "customer/index";
    }

    @PostMapping("/update/{id}")
    public String update(@PathVariable("id") long id, @Valid Customer customer,
                         BindingResult result, Model model) {
        if (result.hasErrors()) {
            customer.setId(id);
            return "customer/update-customer";
        }

        customerRepo.save(customer);
        model.addAttribute("customers", customerRepo.findAll());
        return "customer/index";
    }

    @GetMapping("/delete/{id}")
    public String delete(@PathVariable("id") long id, Model model) {
        Customer customer = customerRepo.findById(id)
                .orElseThrow(() -> new IllegalArgumentException("Invalid customer Id:" + id));
        customerRepo.delete(customer);
        model.addAttribute("customers", customerRepo.findAll());
        return "customer/index";
    }

}
