-- Create New Database with name : alfa
-- username : postgres
-- password : abdulaja (you can change this) see in application.yml

-- Creat Sequence
CREATE SEQUENCE IF NOT EXISTS customer_seq
	INCREMENT BY 1
    MINVALUE 1
	MAXVALUE 999999999
    START 1
	NO CYCLE;

-- Create Table
CREATE TABLE customer (
	id BIGINT DEFAULT NEXTVAL('customer_seq') PRIMARY KEY,
	name VARCHAR NOT NULL,
	email VARCHAR NOT NULL
);